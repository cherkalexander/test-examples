import React, { useState } from 'react';
import { usePopper } from 'react-popper';

export function DropDownButton() {
  const [referenceElement, setReferenceElement] =
    useState<HTMLButtonElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
    null
  );
  const [arrowElement, setArrowElement] = useState<HTMLDivElement | null>(null);
  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [{ name: 'arrow', options: { element: arrowElement } }]
  });
  const [isOpened, setIsOpened] = useState(false);

  return (
    <>
      <button
        type="button"
        ref={setReferenceElement}
        onClick={() => setIsOpened(!isOpened)}
      >
        Reference element
      </button>

      {isOpened && (
        <div
          ref={setPopperElement}
          style={{
            ...{
              border: '1px solid dark',
              backgroundColor: 'white',
              color: 'black'
            },
            ...styles.popper
          }}
          {...attributes.popper}
        >
          I'm layer
          <div ref={setArrowElement} style={styles.arrow} />
        </div>
      )}
    </>
  );
}

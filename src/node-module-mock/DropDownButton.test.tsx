import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { DropDownButton } from './DropDownButton';

describe('DropDownButton', () => {
  test('layer is opened', async () => {
    render(<DropDownButton />);
    userEvent.click(screen.getByRole('button'));
    expect(await screen.findByText(/layer/)).toMatchSnapshot();
  });
});

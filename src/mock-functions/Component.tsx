export interface ComponentProps {
  onClick: () => void;
}

export function Component({ onClick }: ComponentProps) {
  return <button onClick={onClick} />;
}

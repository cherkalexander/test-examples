import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Component } from './Component';

const doAdd = (a: number, b: number, callback: (sum: number) => void) => {
  callback(a + b);
};

describe('jest.fn examples', () => {
  test('onClick works', () => {
    const clickHandler = jest.fn();
    render(<Component onClick={clickHandler} />);

    userEvent.click(screen.getByRole('button'));

    expect(clickHandler).toBeCalled();
  });

  test('calls callback with arguments added', () => {
    const mockCallback = jest.fn();
    doAdd(1, 2, mockCallback);
    expect(mockCallback).toHaveBeenCalledWith(3);
  });
});

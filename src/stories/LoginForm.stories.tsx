import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { LoginForm } from './LoginForm';
import { userEvent, within } from '@storybook/testing-library';

export default {
  title: 'Example/LoginForm',
  component: LoginForm,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen'
  }
} as ComponentMeta<typeof LoginForm>;

const Template: ComponentStory<typeof LoginForm> = () => <LoginForm />;

export const Default = Template.bind({});
Default.play = async ({ canvasElement }) => {
  // Starts querying the component from its root element
  const canvas = within(canvasElement);

  await userEvent.type(
    canvas.getByPlaceholderText('Email'),
    'email@provider.com',
    {
      delay: 100
    }
  );
  await userEvent.type(
    canvas.getByPlaceholderText('Password'),
    'a-random-password',
    {
      delay: 100
    }
  );

  // See https://storybook.js.org/docs/react/essentials/actions#automatically-matching-args to learn how to setup logging in the Actions panel
  await userEvent.click(canvas.getByRole('button'));
};

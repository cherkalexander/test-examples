import { DataResponseItem } from '../ApiClient';

export async function fetchData(): Promise<DataResponseItem[]> {
  return new Promise<DataResponseItem[]>((resolve) => {
    setTimeout(
      () => resolve([{ Name: 'Mocked Data', Type: 'Mocked Music' }]),
      10
    );
  });
}

import { render, screen } from '@testing-library/react';
import { ApiComponent } from './ApiComponent';

// it is necessary for manual mocking of internal stuff to call 'jest.mock'
// https://jestjs.io/docs/manual-mocks
jest.mock('./ApiClient');

describe('Manual mock', () => {
  test('Recommendations are shown', async () => {
    render(<ApiComponent />);
    expect(await screen.findByRole('list')).not.toBeEmptyDOMElement();
    screen.getByText(/Mocked Data/);
  });
});

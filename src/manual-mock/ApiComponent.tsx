import { useEffect, useState } from 'react';
import { DataResponseItem, fetchData } from './ApiClient';

export function ApiComponent() {
  const [response, setResponse] = useState<DataResponseItem[] | null>(null);

  useEffect(() => {
    fetchData().then((resp) => setResponse(resp));
  }, []);

  return (
    <div className="api-component">
      {!!response && (
        <ul>
          {response.map((item, index) => (
            <li key={index}>
              {item.Name} [{item.Type}]
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

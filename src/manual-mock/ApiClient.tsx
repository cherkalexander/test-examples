export interface DataResponseItem {
  Name: string;
  Type: string;
}

export async function fetchData(): Promise<DataResponseItem[]> {
  return new Promise<DataResponseItem[]>((resolve) => {
    setTimeout(() => resolve([{ Name: 'abc', Type: 'Music' }]), 10);
  });
}

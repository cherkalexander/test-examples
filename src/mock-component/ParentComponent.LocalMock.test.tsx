import { render, screen } from '@testing-library/react';
import * as ChildComponentModule from './ChildComponent';
import { ParentComponent } from './ParentComponent';

describe('ParentComponent', () => {
  test('ChildComponent is mocked', () => {
    const mockedFunction = jest
      .spyOn(ChildComponentModule, 'ChildComponent')
      .mockImplementation(() => <div data-testid="ChildComponent"></div>);
    render(<ParentComponent />);

    screen.getByTestId('ChildComponent');
    expect(mockedFunction).toBeCalledTimes(1);
    mockedFunction.mockRestore();
  });

  describe('mock for group of tests', () => {
    let mockedFunction: jest.SpyInstance;
    beforeEach(() => {
      mockedFunction = jest
        .spyOn(ChildComponentModule, 'ChildComponent')
        .mockImplementation(() => <div data-testid="ChildComponent"></div>);
    });

    afterEach(() => mockedFunction.mockRestore());
    beforeEach(() => mockedFunction.mockClear());

    test('ChildComponent is mocked', () => {
      render(<ParentComponent />);

      screen.getByTestId('ChildComponent');
      expect(mockedFunction).toBeCalledTimes(1);
    });

    test('ChildComponent is still mocked', () => {
      render(<ParentComponent />);

      screen.getByTestId('ChildComponent');
      expect(mockedFunction).toBeCalledTimes(1);
    });
  });

  test('ChildComponent is not mocked', () => {
    const { container } = render(<ParentComponent />);
    expect(container).toHaveTextContent("I'm child component");
  });
});

import { ChildComponent } from './ChildComponent';

export function ParentComponent() {
  return (
    <div className="parent-component">
      <ChildComponent />
    </div>
  );
}

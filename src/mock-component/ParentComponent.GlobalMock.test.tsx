import { render, screen } from "@testing-library/react";
import { ChildComponent } from "./ChildComponent";
import { ParentComponent } from "./ParentComponent";

/**
 * Requires to add following configuration to the "package.json" to have ability to use `jest.fn()`
 * 
 * "jest": {
 *   "resetMocks": false
 * } 
 */
jest.mock('./ChildComponent', () => ({
    ChildComponent: jest.fn(() => (<div data-testid="ChildComponent"></div>))
}));

describe("ParentComponent", () => {
  test("ChildComponent is mocked", () => {
    render(<ParentComponent />);
    screen.getByTestId("ChildComponent");
    expect(ChildComponent).toBeCalled();
  });
});

export function usePopper(
  referenceElement: any,
  popperElement: any,
  options: any
) {
  return {
    styles: {},
    attributes: {
      popper: {
        'data-mocked': 'true'
      }
    }
  };
}

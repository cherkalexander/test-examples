const { exec } = require('child_process');

const startStorybook = exec('npx start-storybook -p 6006 --no-open --ci');
// https://stackoverflow.com/questions/15257729/display-running-child-process-output-in-nodejs-windows
startStorybook.stdout.pipe(process.stdout);
startStorybook.stderr.pipe(process.stderr);
// let isFinished = false;

new Promise((resolve, reject) => {
  startStorybook.stdout.on('data', (data) => {
    if (data.includes('No issues found')) {
      resolve();
    }
  });
  startStorybook.stderr.on('error', (data) => {
    reject();
  });
  startStorybook.on('close', (code) => {
    console.log(`start storybook: child process exited with code ${code}`);
    // if (!isFinished) {
    //   reject();
    // }
  });
}).then(() => {
  console.log('started!!!');
  const testStorybook = exec('npx test-storybook --url http://localhost:6006');
  testStorybook.stdout.pipe(process.stdout);
  testStorybook.stderr.pipe(process.stderr);
  testStorybook.on('error', (error) => {
    testStorybook.kill();
    throw error;
  });
  testStorybook.on('close', (code) => {
    // isFinished = true;
    startStorybook.stdout.unpipe();
    startStorybook.stderr.unpipe();
    if (startStorybook.kill()) {
      console.log('start storybook: killed successfully');
    } else {
      console.log('start storybook: kill failure');
    }
    console.log(`test storybook: child process exited with code ${code}`);
  });
});

//TODO: job is not finished after tests complete
